var Materias = require('../models/materias');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar = function(req, res, next){

	//req.body.usuario = ObjectId(req.body.usuario);
	var materias = new Materias({
		profesor : req.body.profesor,
		curso : req.body.curso,
		codigo : req.body.codigo,
		semestre:req.body.semestre,
		hora:req.body.hora,
		dias:req.body.dias,
		institucion:req.body.institucion
		//usuario : req.session.passport.user._id.toString()
	});
	
	var query1=Materias.find({codigo:materias.codigo, dias:materias.dias});
	var query2=Materias.findOne({codigo:materias.codigo, dias:materias.dias});
	
	query1.exec(function(err,doc){
		//console.log("doc: "+doc);
		if(doc.length==0){
			console.log("no existe");
			materias.save(function (err, materia){
				if (err) {
					console.log("err: "+err);
					res.send({success : false, message : err});
				}else{
					console.log("Guardado con exito!");
					res.send({success: true, materia : materia, exists:false});
				}
			});
			Materias.update({codigo:materias.codigo, dias:materias.dias},
					{$set: {students: []}},
					{safe: true, upsert: true},
    				function(err, model) {
        				console.log(err);
    			});
		}else{
			query2.exec(function(err,doc){
				var student=ObjectId(req.session.passport.user._id.toString()).toString();
				if(doc.students.indexOf(student)!=-1){    //si existe 
					console.log("estudiante ya existe en esta materia");
					res.send({success:true, exists:true, student_exists:true})
				}else{
					Materias.update({codigo:materias.codigo, dias:materias.dias},
						{$push: {students: student}},
						{safe: true, upsert: true},
    					function(err, model) {
        				console.log(err);
    				});
				res.send({success:true, exists:true,materia:doc, student_exists:false});
				};
				
				//console.log("ya existe");
			});
		}
	});
};



exports.getMaterias=function(req,res,next){
	console.log("pepepe");
	Materias.find({})
	.exec(function(err,materias){
		if(err){
			console.log(err);
			//console.log("hahhaha");
		}else{
			//console.log("hahahaha");
			res.send(materias);
		}
	});
};

exports.desinscribir=function(req,res,next){
	var materia=req.body._id;
	console.log(materia);
	
	var query_d=Materias.findOne({_id:materia});
	query_d.exec(function(err,doc){
		var student=ObjectId(req.session.passport.user._id.toString()).toString();
		Materias.update({_id:materia},
			{$pull: {students: student}},
			{safe: true, upsert: true},
    		function(err, model) {
        	console.log(err);
    	});
	})
	
}
