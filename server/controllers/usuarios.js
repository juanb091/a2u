var Usuario = require('../models/usuarios');
var passport = require('../config/passport');
var emailCheck = require('email-check');
var emailExistence=require('../email-existence/index');
var kickbox = require('kickbox').client('6e4b22f3296bb4518103bfea627f78599bf4c118d94b9cc1fc8bf0e1676f72ee').kickbox();

exports.registro = function(req, res, next){
	var usuario = new Usuario(req.body);
	console.log(usuario.email);
	
	kickbox.verify(usuario.email, function (err, response) {
  				// Let's see some results
  		console.log(response.body);
  		var valid=response.body.result;
  		console.log("valid: "+valid);
  		if(valid=='deliverable' || valid=='risky'){
			console.log('entro')
			usuario.save(function (err, usuario){
			if (err) {
				res.send({success : false, message : err});
			}else{
				/*
				kickbox.verify("jp.lagos78@uniandes.edu.co", function (err, response) {
	  				// Let's see some results
	  				console.log(response.body);
				});
				*/
				req.logIn(usuario, function (err){
					if (!err) {
						res.send({logged: true, success: true, usuario : req.session.passport});
					}else{
						console.log(err);
						res.send({logged: false, success: true, usuario : usuario});
					}
				});
			}
		});
		}else{
		res.send({success : false, message : "invalid email", invalid:true});
	}
			});
	
};


exports.login =	function (req, res, next){
	var auth = passport.authenticate('local', function (err, user){
		if (err) {
			console.log(err);
			return next(err);
		}
		if(!user){
			console.log("No hay usuario!");
			res.send({success : false});
		}
		req.logIn(user, function (err){
			if (err) {
				console.log("Error al loguearse!");
				return next(err)
			}
			res.send({success : true, user : user});
		});
	});
	auth(req, res, next);
/*
	emailCheck('albis656@gmail.com')
  	.then(function (res) {
    // Returns "true" if the email address exists, "false" if it doesn't. 
    console.log("res: "+res);
  })
  .catch(function (err) {
    if (err.message === 'refuse') {
      // The MX server is refusing requests from your IP address. 
    } else {
      // Decide what to do with other errors. 
    }
  });

  emailExistence.check('albis656@gmail.com', function(err,res){
		console.log('res2: '+res);
	});

  kickbox.verify("juanb091@hotmail.com", function (err, response) {
  // Let's see some results
  console.log(response.body);
});
  kickbox.verify("jp.lagos78@uniandes.edu.co", function (err, response) {
  // Let's see some results
  console.log(response.body);
});
*/
};

exports.userAuthenticated = function(req, res, next){
	if (req.isAuthenticated()) {
		res.send({user : req.session.passport, isLogged : true});
	}else{
		res.send({user : {}, isLogged : false});
	}
};


exports.logout = function(req, res, next){
	req.session.destroy(function(err){
		console.log("Logout");
		if (!err) {
			res.send({destroy : true});
		}else{
			console.log(err);
		}
	});
};