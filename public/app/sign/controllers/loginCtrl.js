angular.module('MainApp').controller('loginCtrl', function($scope, $http, $state, toastF, Session){
	$scope.master = {};

	$scope.usuario = {}; // registro

	$scope.signin = function(){
		var usuario = {username : $scope.usuario.username, password : $scope.usuario.password};
		Session.logIn(usuario)
		.then(function (response){
			if (response.data.success) {
				toastF.success('Iniciaste sesión correctamente!');
				$state.transitionTo('app.dashboard');
			}else{
				toastF.error('Error de autenticación, verifica tus datos!');
				$scope.usuario = angular.copy($scope.master);
				$scope.form.$setPristine();
			}
		});
	}

	//-------------------------
	// registro
	//-------------------------
	$scope.register = function(){
	var inicio=$scope.institucion.indexOf("@");
	var final=$scope.institucion.length;
	$scope.usuario.institucion=$scope.institucion.substring(inicio,final);
	$scope.usuario.email=$scope.institucion;
	$scope.enviando = true;
	$http.post('/registro' , $scope.usuario)
		.then(function(response){
			var data = response.data;
			if (data.success) {
				if (data.logged) {
					$state.transitionTo('app.dashboard');
				}else{
					
					$state.go('login');
				}
			}else{
				console.log("Error al registrarse!");
				$scope.enviando = false;
			};
			if(data.invalid){
				toastF.error('Invalid e-mail');
			}
		});
	}
	//-------------------------
	//
	//-------------------------

	Session.isLogged()
	.then(function(response){
		var isLogged = response.data.isLogged;
		if (isLogged) {
			$state.go('app.dashboard');
		}
	});

});