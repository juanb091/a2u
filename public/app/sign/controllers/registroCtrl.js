angular.module('MainApp').controller('registroCtrl', function($scope, $http, $state,toastF){

	$scope.usuario = {};

	$scope.register = function(){
		var inicio=$scope.institucion.indexOf("@");
		var final=$scope.institucion.length;
		$scope.usuario.institucion=$scope.institucion.substring(inicio,final);
		$scope.usuario.email=$scope.institucion;
		$scope.enviando = true;
		$http.post('/registro' , $scope.usuario)
		.then(function(response){
			var data = response.data;
			if (data.success) {
				if (data.logged) {
					$state.transitionTo('app.dashboard');
				}else{
					
					$state.go('login');
				}
			}else{
				console.log("Error al registrarse!");
				$scope.enviando = false;
			};
			if(data.invalid){
				toastF.error('Invalid e-mail');
			}
		});
	}
});